package daodto;

public class EmployeeDTO {
	private String empId;
	private String empFname;
	private String empLname;
	private String empPw;
	private int empRole;

	public EmployeeDTO() {
		this.empId = null;
		this.empFname = null;
		this.empLname = null;
		this.empPw = null;
		this.empRole = 0;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpFname() {
		return empFname;
	}

	public void setEmpFname(String empFname) {
		this.empFname = empFname;
	}

	public String getEmpLname() {
		return empLname;
	}

	public void setEmpLname(String empLname) {
		this.empLname = empLname;
	}

	public String getEmpPw() {
		return empPw;
	}

	public void setEmpPw(String empPw) {
		this.empPw = empPw;
	}

	public int getEmpRole() {
		return empRole;
	}

	public void setEmpRole(int empRole) {
		this.empRole = empRole;
	}



}
