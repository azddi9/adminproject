package daodto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class EmployeeDAO {
	private Connection con;

	public EmployeeDAO() throws Exception {
		DBManagerAdmin admin = DBManagerAdmin.getDBManagerAdmin();
		this.con = admin.getConnection();
	}

	public boolean checkDuplicate(String id) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		boolean result = false;

		try {
			ps = con.prepareStatement(SQLResources.checkEmpDuplicate);
			ps.setString(1, id);
			rs = ps.executeQuery();
			if(rs.next()) {
				result = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public void insertEmp(String id, String fName, String lName, String pw, int role) {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement(SQLResources.insertEmp);
			ps.setString(1, id);
			ps.setString(2, fName);
			ps.setString(3, lName);
			ps.setString(4, pw);
			ps.setInt(5, role);

			ps.execute();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public EmployeeDTO getEmpById(String id) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		EmployeeDTO emp = new EmployeeDTO();
		emp.setEmpId(id);

		try {
			ps = con.prepareStatement(SQLResources.getEmpById);
			ps.setString(1, id);
			rs = ps.executeQuery();

			if(rs.next()) {
				emp.setEmpId(rs.getString(1));
				emp.setEmpFname(rs.getString(2));
				emp.setEmpLname(rs.getString(3));
				emp.setEmpPw(rs.getString(4));
				emp.setEmpRole(rs.getInt(5));
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

		return emp;
	}

	public void updatePw(String newPw, String empId) {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement(SQLResources.updatePw);
			ps.setString(1, newPw);
			ps.setString(2, empId);
			ps.execute();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
