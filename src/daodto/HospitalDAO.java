package daodto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class HospitalDAO {
	private Connection con;

	public HospitalDAO() throws Exception {
		DBManagerAdmin admin = DBManagerAdmin.getDBManagerAdmin();
		this.con = admin.getConnection();
	}

	public boolean checkDuplicate(String id) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		boolean result = false;

		try {
			ps = con.prepareStatement(SQLResources.checkHospDuplicate);
			ps.setString(1, id);
			rs = ps.executeQuery();
			if(rs.next()) {
				result = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public void insertHosp(String id, String name, String address, String tel, int capital, int emargency) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(SQLResources.insertHosp);
			ps.setString(1, id);
			ps.setString(2, name);
			ps.setString(3, address);
			ps.setString(4, tel);
			ps.setInt(5, capital);
			ps.setInt(6, emargency);

			ps.execute();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<HospitalDTO> getAllHosp(){
		ArrayList<HospitalDTO> list = new ArrayList<HospitalDTO>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement(SQLResources.getAllHosp);
			rs = ps.executeQuery();

			while (rs.next()) {
				HospitalDTO hosp = new HospitalDTO();
				hosp.setId(rs.getString(1));
				hosp.setName(rs.getString(2));
				hosp.setAddress(rs.getString(3));
				hosp.setTel(rs.getString(4));
				hosp.setCapital(Integer.parseInt(rs.getString(5)));
				hosp.setEmargency(Integer.parseInt(rs.getString(6)));

				list.add(hosp);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<HospitalDTO> getHospByAddress(String address){
		ArrayList<HospitalDTO> list = new ArrayList<HospitalDTO>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		String fixedAddress = "%"+address+"%";

		try {
			ps = con.prepareStatement(SQLResources.getHospByAddress);
			ps.setString(1, fixedAddress);
			rs = ps.executeQuery();

			while (rs.next()) {
				HospitalDTO hosp = new HospitalDTO();
				hosp.setId(rs.getString(1));
				hosp.setName(rs.getString(2));
				hosp.setAddress(rs.getString(3));
				hosp.setTel(rs.getString(4));
				hosp.setCapital(Integer.parseInt(rs.getString(5)));
				hosp.setEmargency(Integer.parseInt(rs.getString(6)));

				list.add(hosp);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public HospitalDTO getHospById(String id) throws Exception{
		PreparedStatement ps = null;
		ResultSet rs = null;
		HospitalDTO hosp = new HospitalDTO();
		hosp.setId(id);

		try {
			ps = con.prepareStatement(SQLResources.getHospById);
			ps.setString(1, id);
			rs = ps.executeQuery();

			if(rs.next()) {
				hosp.setId(rs.getString(1));
				hosp.setName(rs.getString(2));
				hosp.setAddress(rs.getString(3));
				hosp.setTel(rs.getString(4));
				hosp.setCapital(Integer.parseInt(rs.getString(5)));
				hosp.setEmargency(Integer.parseInt(rs.getString(6)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return hosp;
	}
}
