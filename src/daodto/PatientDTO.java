package daodto;

import java.sql.Date;

public class PatientDTO {
	private String id;
	private String fName;
	private String lName;
	private String insName;
	private Date exp;

	public PatientDTO() {
		this.id = null;
		this.fName = null;
		this.lName = null;
		this.insName = null;
		this.exp = null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getInsName() {
		return insName;
	}

	public void setInsName(String insName) {
		this.insName = insName;
	}

	public Date getExp() {
		return exp;
	}

	public void setExp(Date exp) {
		this.exp = exp;
	}
}
