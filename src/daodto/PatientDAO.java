package daodto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class PatientDAO {
	private Connection con;

	public PatientDAO() throws Exception {
		DBManagerAdmin admin = DBManagerAdmin.getDBManagerAdmin();
		this.con = admin.getConnection();
	}

	public boolean checkDuplicate(String id) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		boolean result = false;

		try {
			ps = con.prepareStatement(SQLResources.checkPatDuplicate);
			ps.setString(1, id);
			rs = ps.executeQuery();
			if(rs.next()) {
				result = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public void insertPat(String id, String fName, String lName, String insName, String exp) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(SQLResources.insertPat);
			ps.setString(1, id);
			ps.setString(2, fName);
			ps.setString(3, lName);
			ps.setString(4, insName);
			ps.setString(5, exp);

			ps.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updatePat(String id, String fName, String lName, String insName, String exp) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(SQLResources.updatePat);
			ps.setString(1, fName);
			ps.setString(2, lName);
			ps.setString(3, insName);
			ps.setString(4, exp);
			ps.setString(5, id);

			ps.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<PatientDTO> getPatByName(String lName){
		ArrayList<PatientDTO> list = new ArrayList<PatientDTO>();

		String fixedName = "%"+lName+"%";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement(SQLResources.getPatByName);
			ps.setString(1, fixedName);

			rs = ps.executeQuery();

			while (rs.next()) {
				PatientDTO pat = new PatientDTO();
				pat.setId(rs.getString(1));
				pat.setfName(rs.getString(2));
				pat.setlName(rs.getString(3));

				list.add(pat);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
