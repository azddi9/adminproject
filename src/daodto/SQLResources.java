package daodto;

public class SQLResources {
	final static public String login = "select emprole from employee where empid=? and emppasswd=?";
	final static public String checkEmpDuplicate = "select empid from employee where empid=?";
	final static public String insertEmp = "insert into employee values(?,?,?,?,?)";
	final static public String searchEmp = "select empfname, emplname from employee where empid=?";
	final static public String updateEmp = "update employee set empfname=?, emplname=? where empid=?";
	final static public String getEmpById = "select * from employee where empid=?";
	final static public String checkCurrentPw = "select emppasswd from employee where empid=?";
	final static public String updatePw = "update employee set emppasswd=? where empid=?";


	final static public String checkHospDuplicate = "select tabyouinid from tabyouin where tabyouinid=?";
	final static public String insertHosp = "insert into tabyouin values(?,?,?,?,?,?)";
	final static public String getAllHosp = "select * from tabyouin";
	final static public String getHospByAddress = "select * from tabyouin where tabyouinaddress like ?";
	final static public String getHospById = "select * from tabyouin where tabyouinid=?";
	final static public String updateHospTel = "update tabyouin set tabyouintel=? where tabyouinid=?";


//	final static public String insertShiire = "insert into shiiregyosha values(?,?,?,?,?,?)";
//	final static public String getAllShiire = "select * from shiiregyosha";
//	final static public String getShiireByAddress = "select * from shiiregyosha where shiireAddress  like ?";
//	final static public String getShiireByShihon = "select * from shiiregyosha where shihonkin>=?";
//	final static public String updateShiireTel = "update shiiregyosha set shiiretel=? where shiireid=?";


	final static public String checkPatDuplicate = "select patid from patient where patid=?";
	final static public String insertPat = "insert into patient values(?,?,?,?,?)";
	final static public String updatePat = "update patient set patfname=? patlname=? hokenmei=? hokenexp=? where patid=?";
	final static public String getPatByName = "select * from patient where patlname like ?";//とりあえず苗字のみ


}
