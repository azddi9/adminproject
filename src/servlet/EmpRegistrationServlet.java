package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import daodto.EmployeeDAO;
import daodto.EmployeeDTO;

/**
 * Servlet implementation class EmpRegistrationServlet
 */
@WebServlet("/EmpRegistrationServlet")
public class EmpRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	public void perform(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession(true);

		EmployeeDTO empData = (EmployeeDTO) session.getAttribute("empData");
		String id = empData.getEmpId();
		String fName = empData.getEmpFname();
		String lName = empData.getEmpLname();
		String pw = empData.getEmpPw();
		int role = empData.getEmpRole();

		SaltUserPassword hash = new SaltUserPassword();
		String hashedPw = hash.getDigest(id, pw);

		boolean isDuplicate = true;
		try {
			EmployeeDAO dao = new EmployeeDAO();
			isDuplicate =  dao.checkDuplicate(id);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(isDuplicate) {
			session.setAttribute("title", "従業員登録エラー");
			session.setAttribute("message", "ユーザIDが重複しています。");
			session.setAttribute("url", "empRegistration.jsp");
			req.getRequestDispatcher("/result_error.jsp").forward(req, res);
		}else {
			EmployeeDAO dao;
			try {
				dao = new EmployeeDAO();
				dao.insertEmp(id, fName, lName, hashedPw, role);
			} catch (Exception e) {
				e.printStackTrace();
			}
			session.setAttribute("title", "従業員登録完了");
			session.setAttribute("message", lName+" "+fName+"さんを登録しました。");
			req.getRequestDispatcher("/result_success.jsp").forward(req, res);
		}
	}
}
