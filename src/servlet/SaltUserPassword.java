package servlet;

import org.apache.commons.codec.digest.DigestUtils;

public class SaltUserPassword {
	String SALT = "c4289629b08bc4d61411aaa6d6d4a0c3c5f8c1e848e282976e29b6bed5aeedc7";

	public String getDigest(String id, String pw) {
		String con = SALT + getDigest(id) + getDigest(pw);

		return getDigest(con);
	}

	public String getDigest(String bofore) {
		return DigestUtils.sha256Hex(bofore);
	}
}
