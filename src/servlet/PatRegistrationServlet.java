package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import daodto.PatientDAO;
import daodto.PatientDTO;

/**
 * Servlet implementation class PatRegistrationServlet
 */
@WebServlet("/PatRegistrationServlet")
public class PatRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	public void perform(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession(true);

		PatientDTO patData = (PatientDTO) session.getAttribute("patData");
		String id = patData.getId();
		String fName = patData.getfName();
		String lName = patData.getlName();
		String ins = patData.getInsName();
		String exp = patData.getExp().toString();

		boolean isDuplicate = true;
		try {
			PatientDAO dao = new PatientDAO();
			isDuplicate =  dao.checkDuplicate(id);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(isDuplicate) {
			session.setAttribute("title", "患者登録エラー");
			session.setAttribute("message", "患者IDが重複しています。");
			session.setAttribute("url", "patRegistration.jsp");
			req.getRequestDispatcher("/result_error.jsp").forward(req, res);
		}else {
			PatientDAO dao;
			try {
				dao = new PatientDAO();
				dao.insertPat(id, fName, lName, ins, exp);
			} catch (Exception e) {
				e.printStackTrace();
			}
			session.setAttribute("title", "患者登録完了");
			session.setAttribute("message", lName+" "+fName+"さんを登録しました。");
			req.getRequestDispatcher("/result_success.jsp").forward(req, res);
		}
	}
}
