package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import daodto.HospitalDAO;
import daodto.HospitalDTO;

/**
 * Servlet implementation class HospRegistrationServlet
 */
@WebServlet("/HospRegistrationServlet")
public class HospRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	public void perform(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession(true);

		HospitalDTO hospData = (HospitalDTO) session.getAttribute("hospData");
		String id = hospData.getId();
		String name =  hospData.getName();
		String address = hospData.getAddress();
		String tel = hospData.getTel();
		int capital =  hospData.getCapital();
		int emargency = hospData.getEmargency();

		boolean isDuplicate = true;
		try {
			HospitalDAO dao = new HospitalDAO();
			isDuplicate =  dao.checkDuplicate(id);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(isDuplicate) {
			session.setAttribute("title", "他病院登録エラー");
			session.setAttribute("message", "病院IDが重複しています。");
			session.setAttribute("url", "hospRegistration.jsp");
			req.getRequestDispatcher("/result_error.jsp").forward(req, res);
		}else {
			HospitalDAO dao;
			try {
				dao = new HospitalDAO();
				dao.insertHosp(id, name, address, tel, capital, emargency);
			} catch (Exception e) {
				e.printStackTrace();
			}
			session.setAttribute("title", "他病院登録完了");
			session.setAttribute("message", name+"を登録しました。");
			req.getRequestDispatcher("/result_success.jsp").forward(req, res);
		}
	}

}
