package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import daodto.HospitalDAO;
import daodto.HospitalDTO;

/**
 * Servlet implementation class HospSearchServlet
 */
@WebServlet("/HospSearchServlet")
public class HospSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	public void perform(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession(true);
		String address = req.getParameter("address");

		ArrayList<HospitalDTO> list = null;

		try {
			HospitalDAO dao = new HospitalDAO();
			list = dao.getHospByAddress(address);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (list.isEmpty()) {
			session.setAttribute("title", "病院検索エラー");
			session.setAttribute("message", "指定した住所の病院は存在しません。");
			session.setAttribute("url", "hospSearchByAddress.jsp");
			req.getRequestDispatcher("/result_error.jsp").forward(req, res);
		}else {
			session.setAttribute("hospList", list);
			req.getRequestDispatcher("/hospListDisplay.jsp").forward(req, res);
		}
	}

}
