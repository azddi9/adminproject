package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import daodto.EmployeeDTO;

/**
 * Servlet implementation class EmpDataTransferServlet
 */
@WebServlet("/EmpDataTransferServlet")
public class EmpDataTransferServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	public void perform(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String id = req.getParameter("id");
		String fName = req.getParameter("fName");
		String lName = req.getParameter("lName");
		String pw = req.getParameter("pw");
		int role = Integer.parseInt(req.getParameter("role"));

		EmployeeDTO empData = new EmployeeDTO();
		empData.setEmpId(id);
		empData.setEmpFname(fName);
		empData.setEmpLname(lName);
		empData.setEmpPw(pw);
		empData.setEmpRole(role);

		HttpSession session = req.getSession(true);
		session.setAttribute("empData", empData);
		req.getRequestDispatcher("/empRegistration_confirm.jsp").forward(req, res);
	}
}
