package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import daodto.PatientDAO;
import daodto.PatientDTO;

/**
 * Servlet implementation class PatSearchServlet
 */
@WebServlet("/PatSearchServlet")
public class PatSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	public void perform(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession(true);
		String name = req.getParameter("fName");

		ArrayList<PatientDTO> list = null;

		try {
			PatientDAO dao = new PatientDAO();
			list = dao.getPatByName(name);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (list.isEmpty()) {
			session.setAttribute("title", "患者検索エラー");
			session.setAttribute("message", "指定した名前の患者は存在しません。");
			session.setAttribute("url", "patSearchByName.jsp");
			req.getRequestDispatcher("/result_error.jsp").forward(req, res);
		}else {
			session.setAttribute("patList", list);
			req.getRequestDispatcher("/patListDisplay.jsp").forward(req, res);
		}
	}


}
