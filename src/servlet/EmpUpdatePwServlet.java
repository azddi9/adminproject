package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beansdomain.Employee;
import daodto.EmployeeDAO;

/**
 * Servlet implementation class EmpUpdatePwServlet
 */
@WebServlet("/EmpUpdatePwServlet")
public class EmpUpdatePwServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	public void perform(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String newPw = req.getParameter("newPw");
		String checkPw = req.getParameter("checkPw");

		HttpSession session = req.getSession(true);
		Employee emp =  (Employee) session.getAttribute("empData");
		String empId = emp.getEmpId();

		SaltUserPassword hash = new SaltUserPassword();
		String hashedPw = hash.getDigest(empId, newPw);

		if(!newPw.equals(checkPw)) {
			session.setAttribute("title", "パスワード変更エラー");
			session.setAttribute("message", "入力した確認用パスワードが一致しませんでした。");
			session.setAttribute("url", "empUpdatePw.jsp");
			req.getRequestDispatcher("/result_error.jsp").forward(req, res);

		}else {
			EmployeeDAO dao = null;
			try {
				dao = new EmployeeDAO();
				dao.updatePw(hashedPw, empId);
			} catch (Exception e) {
				e.printStackTrace();
			}
			session.setAttribute("title", "パスワード更新完了");
			session.setAttribute("message", "ユーザID:"+empId+"さんのパスワードを更新しました。");
			req.getRequestDispatcher("/result_success.jsp").forward(req, res);
		}
	}

}
