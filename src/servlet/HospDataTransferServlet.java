package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import daodto.HospitalDTO;

/**
 * Servlet implementation class HospDataTransferServlet
 */
@WebServlet("/HospDataTransferServlet")
public class HospDataTransferServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	public void perform(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String id = req.getParameter("id");
		String name = req.getParameter("name");
		String address = req.getParameter("address");
		String tel = req.getParameter("tel");
		int capital = Integer.parseInt(req.getParameter("capital"));
		int emargency = Integer.parseInt(req.getParameter("emargency"));

		//dtoでやるべきか？
		HospitalDTO hospData = new HospitalDTO();
		hospData.setId(id);
		hospData.setName(name);
		hospData.setAddress(address);
		hospData.setTel(tel);
		hospData.setCapital(capital);
		hospData.setEmargency(emargency);

		HttpSession session = req.getSession(true);
		session.setAttribute("hospData", hospData);
		req.getRequestDispatcher("/hospRegistration_confirm.jsp").forward(req, res);
	}
}
