package servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import daodto.PatientDTO;

/**
 * Servlet implementation class PatDataTransferServlet
 */
@WebServlet("/PatDataTransferServlet")
public class PatDataTransferServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	public void perform(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String id = req.getParameter("id");
		String fName = req.getParameter("fName");
		String lName = req.getParameter("lName");
		String ins = req.getParameter("ins");
		String exp = req.getParameter("exp");

		PatientDTO patData = new PatientDTO();
		patData.setId(id);
		patData.setfName(fName);
		patData.setlName(lName);
		patData.setInsName(ins);
		patData.setExp(Date.valueOf(exp));

		HttpSession session = req.getSession(true);
		session.setAttribute("patData", patData);
		req.getRequestDispatcher("/patRegistration_confirm.jsp").forward(req, res);
	}
}
