package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import daodto.HospitalDAO;
import daodto.HospitalDTO;

/**
 * Servlet implementation class HospReferServlet
 */
@WebServlet("/HospListReferServlet")
public class HospListReferServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	public void perform(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession(true);

		ArrayList<HospitalDTO> list = null;

		try {
			HospitalDAO dao = new HospitalDAO();
			list = dao.getAllHosp();
		} catch (Exception e) {
			e.printStackTrace();
		}

		session.setAttribute("hospList", list);
		req.getRequestDispatcher("/hospListDisplay.jsp").forward(req, res);
	}
}
