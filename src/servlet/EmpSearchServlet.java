package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beansdomain.Employee;

/**
 * Servlet implementation class EmpReferServlet
 */
@WebServlet("/EmpSearchServlet")
public class EmpSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		perform(request, response);
	}

	public void perform(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String empId = req.getParameter("u_id");

		Employee emp = null;
		try {
			emp = new Employee(empId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		HttpSession session = req.getSession(true);
		session.setAttribute("empData", emp);

		if(emp.isExist()) {
			req.getRequestDispatcher("/empUpdatePw.jsp").forward(req, res);
		}else {
			req.getRequestDispatcher("/empRefer.jsp").forward(req, res);
		}
	}

}
