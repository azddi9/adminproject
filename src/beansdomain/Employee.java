package beansdomain;

import java.io.Serializable;

import daodto.EmployeeDAO;
import daodto.EmployeeDTO;

public class Employee implements Serializable {
	private String empId;
	private String empFname;
	private String empLname;
	private String empPw;
	private int empRole;
	private EmployeeDAO dao;

	public Employee() {}

	public Employee(String empId) throws Exception{
		this.dao = new EmployeeDAO();
		EmployeeDTO dto = dao.getEmpById(empId);

		this.empId = dto.getEmpId();
		this.empFname = dto.getEmpFname();
		this.empLname = dto.getEmpLname();
		this.empPw = dto.getEmpPw();
		this.empRole = dto.getEmpRole();
	}

	public boolean isExist() {
		if (this.empFname == null) {
			return false;
		}else {
			return true;
		}
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpFname() {
		return empFname;
	}

	public void setEmpFname(String empFname) {
		this.empFname = empFname;
	}

	public String getEmpLname() {
		return empLname;
	}

	public void setEmpLname(String empLname) {
		this.empLname = empLname;
	}

	public String getEmpPw() {
		return empPw;
	}

	public void setEmpPw(String empPw) {
		this.empPw = empPw;
	}

	public int getEmpRole() {
		return empRole;
	}

	public void setEmpRole(int empRole) {
		this.empRole = empRole;
	}

}
