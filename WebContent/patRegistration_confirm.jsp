<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>患者登録_確認</title>
</head>
<body>
<h2>患者登録_確認</h2>

<jsp:useBean id="patData" class="daodto.PatientDTO" scope="session" />

<form action="PatRegistrationServlet" method="post">
	<table>
		<tr><td>ID</td><td><%=patData.getId() %></td></tr>
		<tr><td>姓</td><td><%=patData.getlName() %></td></tr>
		<tr><td>名</td><td><%=patData.getfName() %></td></tr>
		<tr><td>保険証記号番号</td><td><%=patData.getInsName() %></td></tr>
		<tr><td>保険証有効期限</td><td><%=patData.getExp() %></td></tr>
	</table>
	<br>
	<input type="submit" value="登録">
</form>
<input type="button" value="修正" onclick="history.back()">

</body>
</html>