<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>患者登録</title>
</head>
<body>
<h2>患者登録</h2>

<form action="PatDataTransferServlet" method="post">
	<table>
		<tr><td>ID</td><td><input type="text" name="id"></td></tr>
		<tr><td>姓</td><td><input type="text" name="lName"></td></tr>
		<tr><td>名</td><td><input type="text" name="fName"></td></tr>
		<tr><td>保険証記号番号</td><td><input type="text" name="ins"></td></tr>
		<tr><td>保険証有効期限</td><td><input type="text" name="exp"></td></tr>
	</table>
	<br>
	<input type="submit" value="確認画面へ">
</form>

</body>
</html>