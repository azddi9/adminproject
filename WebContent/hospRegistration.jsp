<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>他病院登録</title>
</head>
<body>

<h2>他病院登録</h2>

<form action="HospDataTransferServlet" method="post">
	<table>
		<tr><td>病院ID</td><td><input type="text" name="id"></td></tr>
		<tr><td>病院名</td><td><input type="text" name="name"></td></tr>
		<tr><td>住所</td><td><input type="text" name="address"></td></tr>
		<tr><td>電話番号</td><td><input type="text" name="tel"></td></tr>
		<tr><td>資本金</td><td><input type="text" name="capital"></td></tr>
		<tr><td>救急対応(1:可/2:不可)</td><td><input type="text" name="emargency"></td></tr>
	</table>
	<br>
	<input type="submit" value="確認画面へ">
</form>

</body>
</html>