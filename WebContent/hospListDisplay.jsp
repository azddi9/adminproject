<%@page import="daodto.HospitalDTO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>他病院一覧</title>
</head>
<body>

<h2>他病院一覧</h2>

<%
@SuppressWarnings("unchecked")
ArrayList<HospitalDTO> hospList = (ArrayList<HospitalDTO>)session.getAttribute("hospList");
%>

<table>
	<tr><th>ID</th><th>病院名</th><th>住所</th><th>電話番号</th><th>資本金</th><th>救急</th></tr>
	<% for(HospitalDTO hosp : hospList){%>
	<tr>
		<td><%=hosp.getId() %></td>
		<td><%=hosp.getName() %></td>
		<td><%=hosp.getAddress() %></td>
		<td><%=hosp.getTel() %></td>
		<td><%=hosp.getCapital() %></td>
		<td><%=hosp.getEmargency() %></td>
	</tr>
	<%} %>
</table>

</body>
</html>