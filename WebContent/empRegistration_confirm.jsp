<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>従業員登録_確認</title>
</head>
<body>

<h2>従業員登録_確認</h2>
<p>以下の内容で登録します、よろしいですか？</p>

<jsp:useBean id="empData"  class="daodto.EmployeeDTO" scope="session" />

<form action="EmpRegistrationServlet" method="post">
	<table>
		<tr><td>ユーザID</td><td><%=empData.getEmpId() %></td></tr>
		<tr><td>姓</td><td><%=empData.getEmpLname() %></td></tr>
		<tr><td>名</td><td><%=empData.getEmpFname() %></td></tr>
		<tr><td>パスワード</td><td>セキュリティのため非表示</td></tr>
		<tr><td>ロール</td><td><%=empData.getEmpRole() %></td></tr>
	</table>
	<br>
	<input type="submit" value="登録">
</form>
<input type="button" value="修正" onclick="history.back()">

</body>
</html>