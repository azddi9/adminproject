<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理者メニュー</title>
</head>
<body>
<h2>管理者メニュー</h2>

<h3>従業員管理</h3>
<a href="empRegistration.jsp">従業員登録</a><br>
<a href="empSearch.jsp">従業員パスワード変更</a><br>

<h3>他病院管理</h3>
<a href="hospRegistration.jsp">他病院登録</a><br>
<a href="HospListReferServlet">他病院一覧</a><br>
<a href="hospSearchByAddress.jsp">住所から病院を検索</a><br>

<hr>

<h2>従業員用機能</h2>
<h3>共通機能</h3>
<a href="patSearchByName.jsp">名前から患者を検索</a><br>

<h3>受付</h3>
<a href="patRegistration.jsp">患者登録</a><br>
<a href="">患者保険証変更</a><br>

<h3>医師</h3>
<a href="">薬投与指示</a><br>
<a href="">薬投与削除</a><br>
<a href="">処置確定</a><br>


</body>
</html>