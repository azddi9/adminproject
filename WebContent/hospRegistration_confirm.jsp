<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>他病院登録_確認</title>
</head>
<body>

<h2>他病院登録_確認</h2>
<p>以下の内容で登録します、よろしいですか？</p>

<jsp:useBean id="hospData"  class="daodto.HospitalDTO" scope="session" />

<form action="HospRegistrationServlet" method="post">
	<table>
		<tr><td>病院ID</td><td><%=hospData.getId() %></td></tr>
		<tr><td>病院名</td><td><%=hospData.getName() %></td></tr>
		<tr><td>住所</td><td><%=hospData.getAddress() %></td></tr>
		<tr><td>電話番号</td><td><%=hospData.getTel() %></td></tr>
		<tr><td>資本金</td><td><%=hospData.getCapital() %></td></tr>
		<tr><td>救急対応(1:可/2:不可)</td><td><%=hospData.getEmargency() %></td></tr>
	</table>
	<br>
	<input type="submit" value="登録">
</form>
<input type="button" value="修正" onclick="history.back()">

</body>
</html>