<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>従業員登録</title>
</head>
<body>

<h2>従業員登録</h2>

<form action="EmpDataTransferServlet" method="post">
	<table>
		<tr><td>ユーザID</td><td><input type="text" name="id"></td></tr>
		<tr><td>姓</td><td><input type="text" name="lName"></td></tr>
		<tr><td>名</td><td><input type="text" name="fName"></td></tr>
		<tr><td>パスワード</td><td><input type="password" name="pw"></td></tr>
		<tr><td>ロール</td><td><input type="text" name="role"></td></tr>
	</table>
	<br>
	<input type="submit" value="確認画面へ">
</form>

</body>
</html>