<%@page import="daodto.PatientDTO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>患者検索結果</title>
</head>
<body>

<h2>患者検索結果</h2>

<%
@SuppressWarnings("unchecked")
ArrayList<PatientDTO> patList = (ArrayList<PatientDTO>)session.getAttribute("patList");
%>

<table>
	<tr><th>ID</th><th>姓</th><th>名</th></tr>
	<% for(PatientDTO pat : patList){%>
	<tr>
		<td><%=pat.getId() %></td>
		<td><%=pat.getlName() %></td>
		<td><%=pat.getfName() %></td>
	</tr>
	<%} %>
</table>

</body>
</html>