<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>従業員パスワード変更</title>
</head>
<body>

<jsp:useBean id="empData"  class="beansdomain.Employee" scope="session" />

<h2>検索結果</h2>

<p>
ユーザID：<jsp:getProperty name="empData" property="empId" />さんのパスワードを変更します。
</p>

<form action="EmpUpdatePwServlet" method="post">
	変更後パスワード：<input type="password" name="newPw"><br>
	確認のため再入力：<input type="password" name="checkPw"><br>
	<input type="submit" value="変更">
</form>

</body>
</html>
